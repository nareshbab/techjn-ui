import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // private djangoApiURL = '/api/';
  // private angularApiURL = 'localhost:8000/api/';
  // While building application change url apiURL value to djangoAPIURL value else angularAPIURL values
  private apiURL = 'http://localhost:8000/api/';

  constructor(private http: HttpClient) { }

  getArticles(): Observable<any> {
    return this.http.get(this.apiURL + 'articles');
  }
}
