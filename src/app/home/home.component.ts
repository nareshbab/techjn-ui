import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  articles: any;
  constructor(private apiService: ApiService) { }
  ngOnInit() {
    this.getArticlesHomePage();
  }

  getArticlesHomePage() {
    this.apiService.getArticles().subscribe((response: {}) => {
      console.log(response);
      this.articles = response['articles'];
    });
  }

}
